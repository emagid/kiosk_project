<?php
namespace Model;

class Product extends \Emagid\Core\Model
{

    public static $tablename = "product";

    public static $fields = [
        "name",
        "sku",
        "price",
        "meta_title",
        "meta_keywords",
        "meta_description",
        "featured_image",
        "colors",
        "shape",
        "clarity",
        "weight",
        "lab",
        "cut_grade",
        "polish",
        "symmetry",
        "fluor",
        "rapaport_price",
        "total",
        "certificate",
        "length",
        "width",
        "depth",
        "depth_percent",
        "table_percent",
        "girdle",
        "culet",
        "description",
        "origin",
        "memo_status",
        "inscription",
        "certificate_file",
        "slug",
        "wholesale_price",
        "video_link",
        "quantity",
        'discount',
        "featured"
    ];
    static $relationships = [
        [
            'name' => 'product_category',
            'class_name' => '\Model\Product_Category',
            'local' => 'id',
            'remote' => 'product_map_id',
            'remote_related' => 'category_id',
            'relationship_type' => 'many'
        ],
        // [
        //   'name'=>'product_collection',
        //   'class_name' => '\Model\Product_Collection',
        //   'local'=>'id',
        //   'remote'=>'product_id',
        //   'remote_related'=>'collection_id',
        //   'relationship_type' => 'many'
        // ],
        // [
        //   'name'=>'product_material',
        //   'class_name' => '\Model\Product_Material',
        //   'local'=>'id',
        //   'remote'=>'product_id',
        //   'remote_related'=>'material_id',
        //   'relationship_type' => 'many'
        // ]
    ];
    private $quality = 85; // JPG image quality 0-100
    private $space = 30; // space between texts
    private $fontSize = 20;

    public static $searchFields = ['name'];

    public static function searchMetaData()
    {
        $data = [];
        $diamonds = self::getList(['where' => 'quantity > 0']);
        foreach($diamonds as $diamond){
            $fieldData = [];
            foreach(self::$searchFields as $field){
                $fieldData[] = $diamond->$field;
            }

            $data[] = ['fields' => $fieldData, 'object' => $diamond];
        }

        return $data;
    }

    public function generateSearchResult()
    {
        return ['name' => $this->name, 'image' => $this->featuredImage(), 'link' => "/products/{$this->slug}"];
    }

    public function beforeValidate()
    {
        if (is_array($this->colors)) {
            $this->colors = implode(',', $this->colors);
        }
        if (is_array($this->sizes)) {
            $this->sizes = implode(',', $this->sizes);
        }
    }

    public static function search($keywords, $limit = 20)
    {
        $sql = "select id, name, featured_image, mpn, price, slug from product where active = 1 and (";
        if (is_numeric($keywords)) {
            $sql .= "id = " . $keywords . " or ";
        }
        $sql .= " lower(name) like '%" . strtolower(urldecode($keywords)) . "%') limit " . $limit;
        return self::getList(['sql' => $sql]);
    }

    public static function getPriceSum($productsStr)
    {
        $total = 0;
        foreach (explode(',', $productsStr) as $productId) {
            $product = self::getItem($productId);
            if($product){
                $total += $product->getPrice();
            }
        }
        return $total;
    }

    public static function getPriceByProducts($products)
    {
        $total = 0;
        if (!$products) {
            return $total;
        }

        foreach ($products as $item) {
            if(is_a($item->product, 'Model\Product')){
                $total += floatval($item->product->getPrice());
            } else {
                if(isset($item->options)){
                    $item->options['stone'] = isset($item->options['stone'])? $item->options['stone']: null;
                    $item->options['metal'] = isset($item->options['metal'])? $item->options['metal']: null;
                    $total += floatval($item->product->getPrice($item->options['metal'], $item->options['stone']));
                }
            }

            if (isset($item->ringMaterial)) {
                $total += floatval($item->ringMaterial->price);
                $total += floatval($item->ringMaterial->size_price($item->size));
            }

            if(isset($item->optionalProduct)){
                if(isset($item->options)){
                    $item->options['stoneId'] = isset($item->options['stoneId'])? $item->options['stoneId']: null;
                    $item->options['metal'] = isset($item->options['metal'])? $item->options['metal']: null;
                    $total += $item->optionalProduct->getPrice($item->options['metal'], $item->options['stoneId']);
                }
            }
            if(isset($item->options)){
                $additionalCharge = Jewelry::$additionalCharge;
                foreach($item->options as $key=>$option){
                    if(isset($additionalCharge[$key])){
                        $total += $additionalCharge[$key][$option];
                    }
                }
            }
        }

        return $total;
    }

    public static function getProductsByIds($ids)
    {
        return self::getList(['where' => ' product.id in (' . implode(',', $ids) . ')']);
    }

    public static function generateToken()
    {
        return md5(uniqid(rand(), true));
    }

    public function featuredImage()
    {
        return FRONT_ASSETS . '/img/diamonds/' . $this->shape . '_diamond.png';
    }

    public function getImages()
    {
        $productImage = new Product_Image();
        $productImage = $productImage->getProductImage($this->id, 'Product');
        $imageArray = [$this->featuredImage()];
        if($productImage){
            // get rid of featured image
            foreach($productImage as $image){
                $imageArray[] = UPLOAD_URL."products/".$image->image;
            }
        }

        return $imageArray;
    }

    public function comboDiamond()
    {
        if(isset($_SESSION['ring'])){
            return true;
        } else {
            return false;
        }
    }

    public function getShapeFullName($shape = null)
    {
        switch($shape?$shape:$this->shape) {
            case 'RB': return 'ROUND';
            case 'AC': return 'ASSCHER';
            case 'CU': return 'CUSHION';
            case 'EM': return 'EMERALD';
            case 'HRT': return 'HEART';
            case 'MAR': return 'MARQUISE';
            case 'OV': return 'OVAL';
            case 'PE': return 'PEAR';
            case 'PR': return 'PRINCESS';
            case 'RAD': return 'RADIANT';
            case 'TRI': return 'TRILLION';
        }
    }
    public function videoLink()
    {
        if($this->video_link){
            return UPLOAD_URL.'products/'.$this->video_link;
        }

        return false;
    }

    public function getPrice()
    {
        $price = floatval($this->price);

        if($this->featured && $this->discount){
            $price = $price * (100 - floatval($this->discount)) / 100;
            $price = floatval($price);
        }

        return $price;
    }

    public function getOriginalPrice()
    {
        return $this->price;
    }

    public function getMap()
    {
        $map = new Product_Map();
        return $map->getMap($this->id, 'Product');
    }

    public static function getDiamondShapes(){
        return [
            'rb'=>'round',
            'pr'=>'princess',
            'em'=>'emerald',
            'ac'=>'asscher',
            'rad'=>'radiant',
            'hrt'=>'heart',
            'cu'=>'cushion',
            'mar'=>'marquise',
            'ov'=>'oval',
            'pe'=>'pear',
            'tri' => 'trillion'
        ];
    }

    public function getUrl()
    {
        return "/product/{$this->slug}";
    }

    public static function randomizer($product, $limit = 10){
        // first try to pick up $limit same shape diamonds
        $sql = "SELECT * FROM product WHERE shape = '{$product->shape}' AND id != {$product->id} AND active = 1 AND quantity >= 1 ORDER BY weight";
        $items = self::getList(['sql' => $sql]);
        if(count($items) < $limit){
            // +- 1 caret diamonds
            $lowerSide = $product->weight - 1;
            $upperSide = $product->weight + 1;
            $sqlClosestCarat = "SELECT * FROM product WHERE weight BETWEEN '$lowerSide' AND '$upperSide' AND active = 1 AND quantity >= 1 ORDER BY weight";
            $caretItems = self::getList(['sql' => $sqlClosestCarat]);
            $items = array_merge($items, $caretItems);
            shuffle($items);
        } else {
            shuffle($items);
        }
        // second, order by the closest caret

        return array_slice($items,0,$limit);
    }

    /**
     * Example url would be /image/diamond?diamond=456&detail=1
     * @param array $params
     * @return array|null
     */
    public function diamondDetailImage()
    {
        $this->createDiamondImages();
        $sourceFileExists = true;
        if(file_exists(UPLOAD_PATH."diamond/phab_dia_template_wiretop_{$this->id}.jpeg") && file_exists(UPLOAD_PATH."diamond/phab_dia_template_wireside_{$this->id}.jpeg")){
            // do nothing
        } else {
            if(!$this->createDiamondImages()){
                $sourceFileExists = false;
            }
        }

        return $sourceFileExists?[UPLOAD_URL."diamond/phab_dia_template_wiretop_{$this->id}.jpeg", UPLOAD_URL."diamond/phab_dia_template_wireside_{$this->id}.jpeg"]:[];

    }

    private function diamondWireTopImage($diamondShape)
    {
        $diamondShape = strtolower($diamondShape);
        return ROOT_DIR."/content/frontend/images/diamond/{$diamondShape}/top.png";
    }

    private function diamondWireSideImage($diamondShape)
    {
        $diamondShape = strtolower($diamondShape);
        return ROOT_DIR."/content/frontend/images/diamond/{$diamondShape}/side.png";
    }

    private function createDiamondImages()
    {
        $textPositionMap = [
            'rb' => [
                'width' => [520, 370],
                'height' => [400, 470],
                'table' => [580, 160],
                'depth' => [800, 530],
                'girdle' => [280, 720],
                'culet' => [630, 700]
            ],
            'em' => [
                'width' => [540, 370],
                'height' => [440, 470],
                'table' => [580, 180],
                'depth' => [870, 560],
                'girdle' => [280, 720],
                'culet' => [630, 720]
            ],
            'mar' => [
                'width' => [500, 370],
                'height' => [440, 510],
                'table' => [580, 190],
                'depth' => [890, 520],
                'girdle' => [280, 700],
                'culet' => [630, 680]
            ],
            'pr' => [
                'width' => [520, 370],
                'height' => [400, 480],
                'table' => [580, 170],
                'depth' => [830, 530],
                'girdle' => [280, 710],
                'culet' => [630, 720]
            ],
            'pe' => [
                'width' => [530, 380],
                'height' => [430, 480],
                'table' => [600, 160],
                'depth' => [860, 550],
                'girdle' => [280, 710],
                'culet' => [690, 720]
            ],
            'ac' => [
                'width' => [540, 370],
                'height' => [440, 470],
                'table' => [580, 150],
                'depth' => [830, 560],
                'girdle' => [310, 710],
                'culet' => [630, 720]
            ],
            'ov' => [
                'width' => [500, 370],
                'height' => [430, 470],
                'table' => [580, 180],
                'depth' => [890, 530],
                'girdle' => [280, 700],
                'culet' => [630, 690]
            ],
            'cu' => [
                'width' => [540, 350],
                'height' => [430, 470],
                'table' => [590, 170],
                'depth' => [860, 560],
                'girdle' => [320, 710],
                'culet' => [650, 720]
            ],
            'heart' => [
                'width' => [520, 370],
                'height' => [400, 470],
                'table' => [600, 160],
                'depth' => [840, 540],
                'girdle' => [300, 690],
                'culet' => [680, 730]
            ],
            'rad' => [
                'width' => [510, 390],
                'height' => [400, 500],
                'table' => [580, 160],
                'depth' => [810, 540],
                'girdle' => [290, 710],
                'culet' => [630, 710]
            ],
            'tri' => [
                'width' => [570, 310],
                'height' => [550, 550],
                'table' => [750, 150],
                'depth' => [1050, 540],
                'girdle' => [320, 710],
                'culet' => [670, 710]
            ],

        ];
        $font = ROOT_DIR.'content/frontend/assets/fonts/fashion/fashion.ttf';
        // create wire top image
        $shape = strtolower($this->shape);
        if(!file_exists($this->diamondWireTopImage($shape)) || !file_exists($this->diamondWireSideImage($this->shape))){
            return false;
        }
        $wireTopImage = imagecreatefrompng($this->diamondWireTopImage($shape));
        $greyColor = imagecolorallocate($wireTopImage, 54, 56, 60);
        $topFileName = UPLOAD_PATH."diamond/phab_dia_template_wiretop_{$this->id}.jpeg";
        $place1 = $textPositionMap[$shape]['width']; // width
        $place2 = $textPositionMap[$shape]['height']; // height
        $width = number_format($this->length, 2).' mm';
        $height = number_format($this->width, 2).' mm';
        imagettftext($wireTopImage, $this->fontSize, 0, $place1[0], $place1[1]+$this->space, $greyColor, $font, $width);
        imagettftext($wireTopImage, $this->fontSize, 0, $place2[0], $place2[1]+$this->space, $greyColor, $font, $height);
        imagejpeg($wireTopImage, $topFileName, $this->quality);

        // create wire side image
        $wireSideImage = imagecreatefrompng($this->diamondWireSideImage($this->shape));
        $greyColor = imagecolorallocate($wireSideImage, 54, 56, 60);
        $sideFileName = UPLOAD_PATH."diamond/phab_dia_template_wireside_{$this->id}.jpeg";
        $place1 = $textPositionMap[$shape]['table']; // table
        $place2 = $textPositionMap[$shape]['depth']; // depth
        $place3 = $textPositionMap[$shape]['girdle']; // girdle
        $place4 = $textPositionMap[$shape]['culet']; // culet
        $table = number_format(floatval($this->table_percent) * 100, 1).'%';
        $depth = number_format(floatval($this->depth_percent) * 100, 1).'%';
        $girdleArray = explode(',', $this->girdle);
        $girdle = $girdleArray[0];
        $culet = $this->culet;
        imagettftext($wireSideImage, $this->fontSize, 0, $place1[0], $place1[1]+$this->space, $greyColor, $font, $table);
        imagettftext($wireSideImage, $this->fontSize, 0, $place2[0], $place2[1]+$this->space, $greyColor, $font, $depth);
        imagettftext($wireSideImage, $this->fontSize - 4, 0, $place3[0], $place3[1]+$this->space, $greyColor, $font, $girdle);
        imagettftext($wireSideImage, $this->fontSize, 0, $place4[0], $place4[1]+$this->space, $greyColor, $font, $culet);
        imagejpeg($wireSideImage, $sideFileName, $this->quality);

        return true;
    }
}
