<?php

namespace Model;

class Vendor_Services extends \Emagid\Core\Model {
  
    static $tablename = "vendor_services";
    public static $fields = ['vendor_id','service_id'];

    static $relationships = [
  	[
  		'name'=>'vendor',
  		'class_name' => '\Model\Vendor',
  		'local'=>'vendor_id',
  		'remote'=>'id',
      	'relationship_type' => 'one'
  	],
  	[
  		'name'=>'service',
  		'class_name' => '\Model\Service',
  		'local'=>'service_id',
  		'remote'=>'id',
      	'relationship_type' => 'one'
  	],
  ];

}
