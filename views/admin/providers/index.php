<?php
 if(count($model->providers)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
           <th width="30%">Email</th>
            <th width="40%">Name</th>
            <th width="40%">Preview</th>
          <th width="15%" class="text-center">Edit</th> 
          <th width="15%" class="text-center">Delete</th> 
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->providers as $obj){ ?>
        <tr>
         <td>
         <a href="<?php echo ADMIN_URL; ?>providers/update/<?php echo $obj->id; ?>">
           <?php echo $obj->email;?>
         </a>
      </td>
      <td>
        <a href="<?php echo ADMIN_URL; ?>providers/update/<?php echo $obj->id; ?>">
           <?php echo $obj->full_name();?>
        </a>
      </td>
         <td>
        <a class="btn-actions"href="<?php echo ADMIN_URL; ?>providers/preview/<?php echo $obj->id; ?>">
         <i class="icon-eye"></i> 
        </a>
      </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>providers/update/<?= $obj->id ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>providers/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'providers';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>