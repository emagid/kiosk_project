<?php
if(count($model->gifs)>0): ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="10%"></th>
                <th width="15%">Image</th>
                <th width="5%">Show in Slider?</th>
                <th width="5%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($model->gifs as $obj){ ?>
                <tr data-gif="<?=$obj->id?>" class="gif">
                    <td></td>
                    <td><img src="<?=$obj->image?>"></td>
<!--                    <td><img src="--><?//=UPLOAD_URL.'Gifs'.DS.$obj->image?><!--"></td>-->
                    <td><input id='gif-<?=$obj->id?>' name="in_slider" value="1" type="checkbox" disabled <?=$obj->in_slider ? 'checked' : ''?>></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?= ADMIN_URL ?>gifs/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'gifs';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script>
    $(document).ready(function(){
        $('.gif').click(function(e){
           var id=$(this).data('gif');
           var in_slider = $('#gif-'+id).prop('checked') ? 0 : 1;
           $.post('/admin/gifs/update', {id:id,in_slider:in_slider},function (data){
               $('#gif-'+id).prop('checked',in_slider);
           });
       });
    });
</script>