<?php if (count($model->weddingbands) > 0): ?>
    <div class="rox">
        <div class="box box-table">
            <table id="data-list" class="table">
                <thead>
                <tr>
                    <th width="40%">Name</th>
                    <th width="8%">14kt Price</th>
                    <th width="8%">18kt Price</th>
                    <th width="8%">Plat Price</th>
                    <th width="15%" class="text-center">Edit</th>
                    <th width="15%" class="text-center">Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($model->weddingbands as $obj){
                    $options = $obj->getOptions()?>
                    <tr class="originalProducts">
                        <td><?php echo $obj->name; ?></td>
                        <td class="text-center">$<?= number_format(current($options)['price']['14kt_gold'], 2) ?></td>
                        <td class="text-center">$<?= number_format(current($options)['price']['18kt_gold'], 2) ?></td>
                        <td class="text-center">$<?= number_format(current($options)['price']['Platinum'], 2) ?></td>
                        <td class="text-center">
                            <a class="btn-actions" href="<?php echo ADMIN_URL; ?>weddingbands/update/<?php echo $obj->id; ?>">
                                <i class="icon-pencil"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a class="btn-actions"
                               href="<?php echo ADMIN_URL; ?>weddingbands/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                               onClick="return confirm('Are You Sure?');">
                                <i class="icon-cancel-circled"></i>
                            </a>
                        </td>

                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <div class="box-footer clearfix">
                <div class='paginationContent'></div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'weddingbands';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;

</script>
<script type="text/javascript">
    $(function () {

    })
</script>
































