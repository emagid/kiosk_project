<style type="text/css">


    @media print {
        /* Стиль для печати */
        h1, h2, p {
            color: #000; /* Черный цвет текста */
        }

        .nav {
            display: none;
        }

        .btn {
            display: none;
        }

        .qqq {
            color: black;
        }

        #general-tab, #billing-info-tab, #shipping-info-tab, #products-tab {
            display: block;
            visibility: visible;
        }

        .form-group {
            margin-bottom: 3px;
        }

        a {
            border: 0;
            text-decoration: none;
        }

        input[type="text"] {
            border-color: white;
        }

        .form-control {
            border: 2px solid blue;
        }

        a img {
            border: 0
        }

        a:after {
            content: " (" attr(href) ") ";
            font-size: 90%;
        }

        a[href^="/"]:after {
            content: " ";
        }
    }
</style>

<? if ($model->order->payment_method == 1) { ?>
    <div class="row">
        <div class="col-md-24">
            <div class="box text-right">
                <a href="<?= ADMIN_URL ?>orders/pay/<?php echo $model->order->id; ?>" class="btn btn-warning">Execute
                    Payment</a>
            </div>
        </div>
    </div>
<? } ?>

<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->order->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div role="tabpanel">
        <p class="qq" style=" text-align:center; color:#F9F9F9;">ORDER #<?php echo $model->order->id; ?></p>


        <div class="tab-content">

            <div role="tabpanel">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>ID order</label>

                                <p>#<?= $model->order->id ?> </p>
                            </div>
                            <div class="form-group">
                                <label>Date of order</label>

                                <p><?= $model->order->insert_time ?> </p>
                            </div>
                            <div class="form-group">
                                <label>Reference Number</label>

                                <p><?= $model->order->ref_num ?> </p>
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <?= $model->form->dropDownListFor('status', \Model\Order::$status, '', ['class' => 'form-control']); ?>
                            </div>
                            <div class="form-group">
                                <label>Customer</label>

                                <div class="input-group">
                                    <? if (is_null($model->order->user)) { ?>
                                        <div class="input-group-addon">
                                            Guest
                                        </div>
                                        <input type="text" disabled="disabled" value="<?= $model->order->email ?>"/>
                                    <? } else { ?>
                                        <div class="input-group-btn">
                                            <a href="<?php echo ADMIN_URL; ?>users/update/<?php echo $model->order->user->id; ?>">
                                                <button type="button" class="btn"><i class="icon-eye"></i></button>
                                            </a>
                                        </div>
                                        <input type="text" disabled="disabled"
                                               value="<?= $model->order->user->email ?>"/>
                                    <? } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Phone Number</label>
                                <?php echo $model->form->textBoxFor('phone', ['id' => 'phone-us']); ?>
                            </div>
                            <div class="form-group">
                                <label>Tracking Number</label>
                                <?php echo $model->form->textBoxFor('tracking_number'); ?>
                            </div>
                            <div class="form-group">
                                <label>Shipping Method</label>
                                <?= $model->form->dropDownListFor('shipping_method', $model->shipping_methods, '', ['class' => 'form-control']); ?>
                            </div>
                            <div class="form-group">
                                <label>Payment Method</label>
                                <?= $model->form->dropDownListFor('payment_method', [1 => 'Credit Card', 2 => 'Bank Wire'], '', ['class' => 'form-control']); ?>
                            </div>
                            <div class="row cc-info"
                                 style="<?= ($model->order->payment_method != 1) ? 'display:none;' : ''; ?>">
                                <div class="col-xs-10 cc_number">
                                    <label>Credit Card #</label>

                                    <div class="input-group">
                                        <?php echo $model->form->textBoxFor('cc_number'); ?>
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-default">Show</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-10">
                                    <div class="col-xs-24">
                                        <label>Expiration</label>
                                    </div>
                                    <div>
                                        <div class="col-xs-16">
                                            <?= $model->form->dropDownListFor('cc_expiration_month', get_month(), '', ['class' => 'form-control']); ?>
                                        </div>
                                        <div class="col-xs-8">
                                            <?= $model->form->dropDownListFor('cc_expiration_year', range(date('Y') - 15, date('Y') + 9), '', ['class' => 'form-control']); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <label>CCV</label>
                                    <?php echo $model->form->textBoxFor('cc_ccv'); ?>
                                </div>
                            </div>
                            <? if (!is_null($model->order->coupon_code)) { ?>
                                <div class="form-group">
                                    <label>Coupon</label>

                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <a href="<?php echo ADMIN_URL; ?>coupons/update/<?php echo $model->coupon->id; ?>">
                                                <button type="button" class="btn"><i class="icon-eye"></i></button>
                                            </a>
                                        </div>
                                        <input type="text" disabled="disabled" value="<?= $model->coupon->code ?>"/>
                                    </div>
                                </div>
                            <? } else {?>
                                <div class="form-group">
                                    <label>Apply Coupon</label>
                                    <select name="apply_coupon" class="form-control">
                                        <option value="">No Coupon</option>
                                        <?foreach(\Model\Coupon::getList(['where'=>"active = 1 and min_amount <= '{$model->order->subtotal}'"]) as $coupon){?>
                                            <option value="<?=$coupon->id?>"><?=$coupon->name.' - '.$coupon->code?></option>
                                        <?}?>
                                    </select>
                                </div>
                            <? } ?>
                            <table class="table" style="margin-top: 10px;">
                                <tr>
                                    <td>Subtotal</td>
                                    <td>$<?php echo number_format(floor($model->order->subtotal * 100) / 100, 2) ?></td>
<!--                                    <td>$--><?php //echo number_format($model->order->subtotal, 2); ?><!--</td>-->
<!--                                    <td>$--><?php //echo $model->order->subtotal; ?><!--</td>-->
                                </tr>
                                <tr>
                                    <td>Discount</td>
                                    <td>$<?= number_format(floor($model->savings * 100) / 100, 2) ?></td>
<!--                                    <td>$--><?//= number_format($model->savings, 2); ?><!--</td>-->
<!--                                    <td>$--><?//= $model->savings; ?><!--</td>-->
                                </tr>
                                <tr>
                                    <td>Tax</td>
                                    <td>$<?php echo number_format(floor($model->order->tax * 100) / 100, 2) ?></td>
<!--                                    <td>$--><?php //echo number_format($model->order->tax, 2); ?><!--</td>-->
<!--                                    <td>$--><?php //echo $model->order->tax; ?><!--</td>-->
                                </tr>
                                <tr>
                                    <td>Shipping</td>
                                    <td>$<?php echo number_format(floor($model->order->shipping_cost * 100) / 100, 2) ?></td>
<!--                                    <td>$--><?php //echo number_format($model->order->shipping_cost, 2); ?><!--</td>-->
<!--                                    <td>$--><?php //echo $model->order->shipping_cost; ?><!--</td>-->
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td>$<?= number_format(floor($model->order->total * 100) / 100, 2) ?></td>
<!--                                    <td>$--><?//= number_format($model->order->total, 2) ?><!--</td>-->
<!--                                    <td>$--><?//= $model->order->total?><!--</td>-->
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?
                    $shp = array($model->order->ship_first_name, $model->order->ship_last_name, $model->order->ship_address, $model->order->ship_address2,
                        $model->order->ship_country, $model->order->ship_city, $model->order->ship_state, $model->order->ship_zip);


                    $blng = array($model->order->bill_first_name, $model->order->bill_last_name, $model->order->bill_address, $model->order->bill_address2,
                        $model->order->bill_country, $model->order->bill_city, $model->order->bill_state, $model->order->bill_zip);

                    ?>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Billing
                                <? if (count(array_diff($shp, $blng)) == 0) {
                                    echo " and shipping <center><font color='green'>SAME</font></center>";
                                } ?>
                            </h4>

                            <div class="form-group">
                                <label>First Name</label>
                                <?php echo $model->form->textBoxFor('bill_first_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <?php echo $model->form->textBoxFor('bill_last_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <?php echo $model->form->textBoxFor('bill_address'); ?>
                            </div>
                            <div class="form-group">
                                <label>Address 2</label>
                                <?php echo $model->form->textBoxFor('bill_address2'); ?>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <?php echo $model->form->textBoxFor('bill_city'); ?>
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <?= $model->form->dropDownListFor('bill_state', get_states(), 'Select', ['class' => 'form-control']); ?>
                            </div>
                            <div class="form-group">
                                <label>Zip Code</label>
                                <?php echo $model->form->textBoxFor('bill_zip'); ?>
                            </div>
                            <div class="form-group">
                                <label>Country</label>
                                <?= $model->form->dropDownListFor('bill_country', get_countries(), '', ['class' => 'form-control']); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 <?if(count(array_diff($shp, $blng)) == 0){ echo "hidden";}?>" >
                        <div class="box">
                            <h4>Shipping</h4>
                            <div class="form-group hidden">
                                <input name="bill_ship_same" value="<?if(count(array_diff($shp, $blng)) == 0){ echo 1;} else { echo 0;}?>">
                            </div>
                            <div class="form-group">
                                <label>First Name</label>
                                <?php echo $model->form->textBoxFor('ship_first_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <?php echo $model->form->textBoxFor('ship_last_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <?php echo $model->form->textBoxFor('ship_address'); ?>
                            </div>
                            <div class="form-group">
                                <label>Address 2</label>
                                <?php echo $model->form->textBoxFor('ship_address2'); ?>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <?php echo $model->form->textBoxFor('ship_city'); ?>
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <?= $model->form->dropDownListFor('ship_state', get_states(), 'Select', ['class' => 'form-control']); ?>
                            </div>
                            <div class="form-group">
                                <label>Zip Code</label>
                                <?php echo $model->form->textBoxFor('ship_zip'); ?>
                            </div>
                            <div class="form-group">
                                <label>Country</label>
                                <?= $model->form->dropDownListFor('ship_country', get_countries(), '', ['class' => 'form-control']); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="box">
                            <h4>Products</h4>
                            <table class="table">
                                <thead>
                                <th>Image</th>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Quantity</th>
                                <th>Unit Price</th>
                                <th>Metal, Color, Size, Shape</th>
                                <th>Total</th>
                                <th>Order</th>
                                <th>View</th>
                                </thead>
                                <tbody>
                                <? foreach ($model->order_products as $order_product) {
                                    $order_product_option = $order_product->getOptions();
                                    $data = $order_product_option? json_decode($order_product_option->data) : ''?>
                                    <tr data-order_product_id = <?=$order_product->id ?>>
                                        <td><img class="large-thumbnail-style" src="<?=$order_product->product()->featuredImage() ?>"/></td>
                                        <td><?= $order_product->product()->id ?></td>
                                        <td><?= $order_product->product()->name ?></td>
                                        <td><?= $order_product->quantity ?></td>
                                        <td>$<?= $order_product->unit_price ?></td>
                                        <?if($order_product_option){?>
                                            <td><?php echo $order_product_option->getMetal()?:$order_product_option->getRingMaterial()?>, <?=$order_product_option->getColor()?:'None'?>, <?=$order_product_option->getSize()?>, <?=$order_product_option->getShape()?></td>
                                        <?} else {?>
                                            <td></td>
                                        <?}?>
                                        <td>$<?= $order_product->totalPrice() ?></td>
                                        <td><?= isset($data->optionalProduct) ? 'Order Combo '.$order_product->id : $order_product->id?></td>
                                        <td><a class="btn-actions" href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->product()->id; ?>"><i class="icon-eye"></i></a></td>
                                    </tr>
                                    <?if(isset($order_product_option) && $order_product_option->order_product_id === $order_product->id){?>
                                    <tr data-order_product_id = <?=$order_product->id ?>>
                                    <?if($ringMaterial = $order_product_option->getRingMaterial()){?>
                                        <td><img class="large-thumbnail-style" src="<?= FRONT_ASSETS ?>img/ring10.png"/></td>
                                        <td><?= $ringMaterial->id ?></td>
                                        <td><?= ucfirst($ringMaterial->name) ?></td>
                                        <td>1</td>
                                        <td>$<?= $ringMaterial->price ?></td>
                                        <td> </td>
                                        <td>$<?= $order_product->totalPrice()?></td>
                                        <td><?= isset($order_product_option) ? 'Order Combo '.$order_product->id : ""?></td>
                                    <? } else if($order_product_option && $optionalProduct = $order_product_option->getProduct()) {?>
                                        <td><img class="large-thumbnail-style" src="<?= FRONT_ASSETS ?>img/ring10.png"/></td>
                                        <td><?= $optionalProduct->id ?></td>
                                        <td><?= $optionalProduct->name ?></td>
                                        <td>1</td>
                                        <td>$<?= $optionalProduct->price ?></td>
                                        <td> </td>
                                        <td>$<?= $order_product->totalPrice()?></td>
                                        <td><?= isset($order_product_option) ? 'Order Combo '.$order_product->id : ""?></td>
                                        <td><a class="btn-actions" href="<?php echo ADMIN_URL; ?>rings/update/<?php echo $optionalProduct->id?>"><i class="icon-eye"></i></a></td>
                                    <? } ?>
                                    </tr>
                                    <? } ?>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    <input type="hidden" id="act_for_click" name="redirectTo" value="orders/">
    <center style="    margin-bottom: 112px;">
        <button type="submit" id="save_close" class="btn btn-save">Save and close</button>
        <button type="submit" id="save" class="btn btn-save">Save</button>
        <button class="btn btn-save" id="print">Print</button>
    </center>
</form>
<?php footer(); ?>

<style>
    div.mouseover-thumbnail-holder {
        position: relative;
        display: block;
        float: left;
        margin-right: 10px;
    }

    .large-thumbnail-style {
        width: 50px;
        display: block;
        border: 2px solid #fff;
        box-shadow: 0px 0px 5px #aaa;
    }

    div.mouseover-thumbnail-holder .large-thumbnail-style {
        position: absolute;
        top: 0;
        left: -9999px;
        z-index: 1;
        opacity: 0;
        transition: opacity .5s ease-in-out;
        -moz-transition: opacity .5s ease-in-out;
        -webkit-transition: opacity .5s ease-in-out;
    }

    div.mouseover-thumbnail-holder:hover .large-thumbnail-style {
        width: 100% !important;
        top: 0;
        left: 105%;
        z-index: 1;
        opacity: 1;

    }

    .combo {
        transform: scale(1.05);
    }
</style>
<script>


    $(function () {
        $('select[name="payment_method"]').on('change', function () {
            if ($(this).val() == 1) {
                $('.cc-info').show();
            } else {
                $('.cc-info').hide();
            }
        });
        $('.cc_number button').click(function () {
            var pwd = prompt("Please enter the code");
            if (pwd != null) {
                $.ajax({
                    'type': 'POST',
                    'url': '<?=ADMIN_URL?>orders/getCcNumber',
                    'data': 'password=' + pwd + '&order_id=<?=$model->order->id?>',
                    'success': function (data) {
                        data = JSON.parse(data);
                        if (data == "false") {
                            alert('Incorrect code.');
                        } else {
                            $('.cc_number input').val(data);
                        }
                    }
                })
            }
        });

        $('tr').hover(function(){
            var order_id = $(this).data('order_product_id');
            if(order_id) {
                $("[data-order_product_id='" + order_id + "']").toggleClass('combo');
            }
        });

        $('#save_close').mouseover(function () {
            $("#act_for_click").val("orders/");

        });

        $('#save').mouseover(function () {
            var id = $("input[name=id]").val();
            $("#act_for_click").val("orders/update/" + id);

        });


    });
    $("#phone-us").inputmask("+1(999)999-9999");
</script>
<script>
    $(function () {
        $('#print').click(function (e) {
            e.preventDefault();
            window.print();
            return false;
        })
    })
</script>






























