$(document).ready(function() {
	var $window = $(this);
	var scrollTop = $window.scrollTop();
    fix_filter_nav(scrollTop);
    $window.scroll(function(e){
        var scrollTop = $(this).scrollTop();
        fix_filter_nav(scrollTop);
    });

    function fix_filter_nav(scrollTop){
        var $category_nav = $(".category_nav");
        var $category_content = $(".category_page_content_wrapper");
        if(scrollTop <= 124){
            $category_nav.css({"position":"relative","top":"0","left":"0","right":"0"});
            $category_content.css({"margin-top":"0"});
        } else{
            $category_nav.css({"position":"fixed","top":"33px","left":"1.5%","right":"1.5%"});
            $category_content.css({"margin-top":"72px"});
        }
    } 

    $(".selected_size").click(function(event){
      event.stopPropagation();
      $(".size_dropdown_box").addClass("active");
    });

    
      $("body").click(function(){
        if($(".size_dropdown_box").hasClass("active")){
          $(".size_dropdown_box").removeClass("active");  
        }
      }); 

    $(".size_dropdown_box li").click(function(){
      $(".size_dropdown_box li.selected").removeClass("selected");
      var selected_size = $(this).text();
      $(this).addClass("selected");
      $(".selected_size").text(selected_size);
    });


    $('.collections_slick_slider').slick({
      dots: false,
      infinite: true,
      pagination:false,
      slidesToShow: 4,
      touchThreshold:20,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 690,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 590,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }

      ]
    });   
});