$(document).ready(function(){

    // DISABLE RIGHT CLICKS
    document.addEventListener('contextmenu', event => event.preventDefault());


    // TIMEOUT
    var initial = null;

    function invoke() {
        initial = window.setTimeout(
            function() {
                setTimeout(function(){
                    // $('.timeout').fadeIn();
                }, 1500);
            }, 60000);
    }


    invoke();

    $('body').on('click mousemove', function(){
        window.clearTimeout(initial);
        invoke();
    })

    $('.timeout').click(function(){
        window.location = '/';
    })
});



