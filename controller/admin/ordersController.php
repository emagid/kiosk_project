<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 11/5/15
 * Time: 6:53 PM
 */
use Emagid\Html\Form;

require_once(ROOT_DIR."libs/authorize/AuthorizeNet.php");

class ordersController extends adminController {

    function __construct(){
        parent::__construct("Order");
    }

    public function index(Array $params = []){
        //$order_by=$_GET['order'];
        //$limit=$_GET['limit'];
        if(Empty($_GET['sort']) ||  Empty($_GET['sort_param']))
        {

            $params['queryOptions']['orderBy'] = "insert_time DESC";
        }else{
            $sort_by=$_GET['sort'];
            $sort_param=$_GET['sort_param'];
            $params['queryOptions']['orderBy'] =   $sort_by.' '. $sort_param;   }

        if (!Empty($_GET['status_show']))
        {
            $st=str_replace ('%20',' ',$_GET['status_show']);
            $params['queryOptions']['where' ] = " status = '$st'";

        }

        parent::index($params);
    }

    public function beforeLoadIndexView(){
        $this->_viewData->products = [];
        foreach($this->_viewData->orders as $order){
            $order->insert_time = new DateTime($order->insert_time);
            $order->insert_time = $order->insert_time->format('m-d-Y H:i:s');
            $order_products = \Model\Order_Product::getList(['where'=>'order_id = '.$order->id]);
            $this->_viewData->products[$order->id] = [];
            foreach($order_products as $order_product){
                $product = $order_product->product();
                $this->_viewData->products[$order->id][] = $product;
            }
        }
    }

    public function beforeLoadUpdateView(){
        if (is_null($this->_viewData->order->viewed) || !$this->_viewData->order->viewed){
            $this->_viewData->order->viewed = true;
            $this->_viewData->order->save();
        }

        $this->_viewData->order_status = \Model\Order::$status;

        $shipping_methods = \Model\Shipping_Method::getList(['orderBy'=>'shipping_method.id']);
        $this->_viewData->shipping_methods = [];
        foreach($shipping_methods as $shipping_method){
            $this->_viewData->shipping_methods[$shipping_method->id] = $shipping_method->name;
        }

        $order_products = $this->_viewData->order->orderProducts();
        $this->_viewData->order_products = $order_products;

        if (is_null($this->_viewData->order->user_id) || $this->_viewData->order->user_id == 0){
            $this->_viewData->order->user = null;
        } else {
            $this->_viewData->order->user = \Model\User::getItem($this->_viewData->order->user_id);
        }

        if (is_null($this->_viewData->order->coupon_code)){
            $this->_viewData->coupon = null;
            $this->_viewData->savings = '0';
        } else {
            $this->_viewData->coupon = \Model\Coupon::getItem(null, ['where'=>"code = '".$this->_viewData->order->coupon_code."'"]);
            if ($this->_viewData->order->coupon_type == 1){//$
                $this->_viewData->savings = $this->_viewData->order->coupon_amount;
            } else if ($this->_viewData->order->coupon_type == 2){//%
                $this->_viewData->savings = $this->_viewData->order->subtotal * $this->_viewData->order->coupon_amount/100;
            }
        }

        if (!is_null($this->_viewData->order->cc_number)){
            $this->_viewData->order->cc_number = '****'.substr($this->_viewData->order->cc_number, -4);
        }

        $this->_viewData->order->cc_expiration_year = '20'.$this->_viewData->order->cc_expiration_year;
    }

    public function getCcNumber(){
        if (isset($_POST['password']) && $_POST['password'] == 'test5343'){
            echo json_encode(\Model\Order::getItem($_POST['order_id'])->cc_number);
        } else {
            echo json_encode("false");
        }
    }

    public function update_post() {
        if (!isset($_POST['id']) || !is_numeric($_POST['id']) || $_POST['id'] < 0){
            redirect(ADMIN_URL.$_POST['redirectTo']);
        }

        $order = \Model\Order::getItem($_POST['id']);

//        if ($order->status != $_POST['status'] && $_POST['status'] == \Model\Order::$status[3] && trim($_POST['tracking_number']) != ""){
//            $this->sendShippedEmail($order, $_POST['tracking_number']);
//        }

        $unchangedStatus = null;
        if($order->status != $_POST['status']) {
            $order->status = $_POST['status'];
        } else {
            $unchangedStatus = $_POST['status'];
        }

//        if($_POST['status'] !== $_POST['old_status']){
//            $log_status =new \Model\Log_Status();
//            $log_status->order_id = $_POST['id'];
//            $log_status->status = $_POST['status'];
//            $log_status->save();
//        }
        $order->tracking_number = $_POST['tracking_number'];
        if ('****'.substr($order->cc_number, -4) != $_POST['cc_number']){
            $order->cc_number = $_POST['cc_number'];
        }
        $order->cc_expiration_month = $_POST['cc_expiration_month'];
        $order->cc_expiration_year = $_POST['cc_expiration_year'];
        $order->cc_ccv = $_POST['cc_ccv'];
        $order->payment_method = $_POST['payment_method'];

        $order->bill_first_name = $_POST['bill_first_name'];
        $order->bill_last_name = $_POST['bill_last_name'];
        $order->bill_address = $_POST['bill_address'];
        $order->bill_address2 = $_POST['bill_address2'];
        $order->bill_city = $_POST['bill_city'];
        $order->bill_state = $_POST['bill_state'];
        $order->bill_country = $_POST['bill_country'];
        $order->bill_zip = $_POST['bill_zip'];

        if($_POST['bill_ship_same']){
            $order->ship_first_name = $order->bill_first_name;
            $order->ship_last_name = $order->bill_last_name;
            $order->ship_address = $order->bill_address;
            $order->ship_address2 = $order->bill_address2;
            $order->ship_city = $order->bill_city;
            $order->ship_state = $order->bill_state;
            $order->ship_country = $order->bill_country;
            $order->ship_zip = $order->bill_zip;
        } else {
            $order->ship_first_name = $_POST['ship_first_name'];
            $order->ship_last_name = $_POST['ship_last_name'];
            $order->ship_address = $_POST['ship_address'];
            $order->ship_address2 = $_POST['ship_address2'];
            $order->ship_city = $_POST['ship_city'];
            $order->ship_state = $_POST['ship_state'];
            $order->ship_country = $_POST['ship_country'];
            $order->ship_zip = $_POST['ship_zip'];
        }

        $order->phone = $_POST['phone'];

        if(isset($_POST['apply_coupon']) && $_POST['apply_coupon']){
            $coupon = \Model\Coupon::getItem($_POST['apply_coupon']);
            $order->coupon_code = $coupon->code;
            $order->coupon_type = $coupon->discount_type;
            $order->coupon_amount = $coupon->discount_amount;
        }

        $subtotal = $order->subtotal;
        if (!is_null($order->coupon_code)){
            if ($order->coupon_type == 1){
                $subtotal = $subtotal - $order->coupon_amount;
            } else if ($order->coupon_type == 2) {
                $subtotal = $subtotal * (1 - ($order->coupon_amount/100));
            }
        }

        /*$logged_admin = \Model\Admin::getItem(\Emagid\Core\Membership::userId());
        $orderchange = new \Model\Orderchange();
        $orderchange->order_id = $_POST['id'];
        $orderchange->user_id =  $logged_admin->id;
        $orderchange->save() ; */



        if ($order->shipping_method != $_POST['shipping_method']){
            $order->shipping_method = \Model\Shipping_Method::getItem($_POST['shipping_method']);
            $costs = json_decode($order->shipping_method->cost);
            $order->shipping_method->cost = 0;
            $order->shipping_method->cart_subtotal_range_min = json_decode($order->shipping_method->cart_subtotal_range_min);
            foreach($order->shipping_method->cart_subtotal_range_min as $key=>$range){
                if (is_null($range) || trim($range) == ''){
                    $range = 0;
                }
                if ($subtotal >= $range && isset($costs[$key])){
                    $order->shipping_cost = $costs[$key];
                }
            }
            $order->shipping_method = $order->shipping_method->id;
        }

        if ($order->ship_state == 'NY'){
            $order->tax_rate = 8.875;
            $order->tax = $subtotal * ($order->tax_rate/100);
        } else {
            $order->tax_rate = null;
            $order->tax = null;
        }

        $order->total = $subtotal + $order->shipping_cost + $order->tax;

        if ($order->save()){
            $n = new \Notification\MessageHandler('Order saved.');
            $_SESSION["notification"] = serialize($n);
        } else {
            $n = new \Notification\ErrorHandler($order->errors);
            $_SESSION["notification"] = serialize($n);
            redirect(ADMIN_URL.$this->_content.'/update/'.$order->id);
        }

        if(!isset($unchangedStatus)){
            $toEmail = null;
            if (is_null($order->user_id)) {
                $toEmail = $order->email;
            } else {
                $toEmail = \Model\User::getItem($order->user_id)->email;
            }

            $email = new Email\MailMaster();

            if($order->status == 'Shipped') {
                $order->sendOrderSuccessEmail();
//                $email->setTo(['email' => $toEmail, 'name' => $toEmail,  'type' => 'to'])->setMergeTags(['ORDER_ID'=>$order->ref_num])->setTemplate('order-shipped')->send();
            } elseif($order->status == 'Canceled') {
//                $email->setTo(['email' => $toEmail, 'name' => $toEmail,  'type' => 'to'])->setMergeTags(['ORDER_ID'=>$order->ref_num])->setTemplate('order-canceled')->send();
            }
        }

        redirect(ADMIN_URL.$_POST['redirectTo']);
    }

    public function pay(Array $params = []){
        if (isset($params['id']) && is_numeric($params['id'])){

            $order = \Model\Order::getItem($params['id']);

            if (!is_null($order)){

                $localTransaction = $this->emagid->db->execute('SELECT ref_trans_id FROM transaction WHERE order_id = '.$order->id);

                if (isset($localTransaction[0]) && isset($localTransaction[0]['ref_trans_id'])){
                    $refTransId = $localTransaction[0]['ref_trans_id'];

//                    $transaction = new AuthorizeNetAIM;
//                    $response = $transaction->priorAuthCapture($refTransId);

                    $tran = new Services\PaymentService();
                    $tran->setCommand('cc:capture')->setRefNum($refTransId);
                    $tran->process();

                    if($tran->isSuccess()){
                        $n = new \Notification\MessageHandler('Approved');
                        $_SESSION["notification"] = serialize($n);
                    } else {
                        $n = new \Notification\MessageHandler($tran->getError());
                        $_SESSION["notification"] = serialize($n);
                    }
                }

                redirect(ADMIN_URL.'orders/update/'.$order->id);

            }

        }

        redirect(ADMIN_URL.'orders');
    }

    public function email_body_shipping($order){
        return ''.$order->status;
    }

    public function email_body_canceled($order){
        return ''.$order->status;
    }
}





















































