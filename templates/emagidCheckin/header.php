<div class='cover'>
	<img src="<?= FRONT_ASSETS ?>img/loader.png">
</div>

<div class='timeout'>
	<video src='<?= FRONT_ASSETS ?>img/vid.mp4' autoplay loop muted></video>
	<img src="<?= FRONT_ASSETS ?>img/hand.png">
	<p>Touch to Start!</p>
</div>

<header>
	<a href="/"><img src="<?= FRONT_ASSETS ?>img/logo.svg"></a>
</header>